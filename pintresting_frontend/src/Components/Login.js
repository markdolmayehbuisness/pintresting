import { React, useEffect } from "react";
import { GoogleLogin } from "@react-oauth/google";
import { GoogleOAuthProvider } from "@react-oauth/google";
import { useNavigate } from "react-router-dom";
import { FcGoogle } from "react-icons/fc";
import shareVideo from "../assets/share.mp4";
import logo from "../assets/logo2.png";
import jwt_decode from "jwt-decode";
import { client } from "../client";

function Login() {
  const navigate = useNavigate();
  const clientId = process.env.REACT_APP_GOOGLE_API_TOKEN;
  const responseGoogle = async (response) => {
    const decoded = await jwt_decode(response.credential);
    localStorage.setItem("user", JSON.stringify(decoded.name));
    localStorage.setItem("googleId", JSON.stringify(decoded.sub));
    // console.log(decoded);
    const { name, sub, picture } = decoded;
    const doc = {
      _id: sub,
      _type: "user",
      userName: name,
      image: picture,
    };
    // console.log(doc);
    client.createIfNotExists(doc).then(() => {
      navigate("/", { replace: true });
    });
  };

  return (
    <div className="flex justify-start items-center flex-col h-screen">
      <div className="relative w-full h-full">
        <video
          src={shareVideo}
          type="video/mp4"
          loop
          controls={false}
          muted
          autoPlay
          className="w-full h-full object-cover "
        ></video>
        <div className="absolute flex flex-col justify-center items-center top-0 right-0 left-0 bottom-0 bg-blackOverlay">
          <div className="p-5">
            <img
              src={logo}
              alt="logo"
              width={"300px"}
              className="rounded-lg -mb-14"
            />
          </div>

          <div className="">
            <GoogleOAuthProvider clientId={clientId}>
              <GoogleLogin
                className="rounded-full"
                onSuccess={responseGoogle}
                onFailure={responseGoogle}
                cookiePolicy="single_host_origin"
              />
            </GoogleOAuthProvider>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Login;
